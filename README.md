Snap IO
=======

Flight controller interface for [`ioboard`](https://gitlab.com/mit-acl/fsw/snap-stack/ioboard). Provides a CPU-side shared library for linking to the [ACL Snap autopilot](https://gitlab.com/mit-acl/fsw/snap-stack/snap) (ROS). Connects to `ioboard` via UART using the sensors dsp (sdsp). Also provides IMU (via `sensor_imu` API and `imu_app` server).

## Building

1. Clone into the [`sfpro-dev`](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev) or [`sf-dev`](https://gitlab.com/mit-acl/fsw/snap-stack/sf-dev) `workspace` directory.
2. Update submodules: `git submodule update --init --recursive`.
3. Follow appropriate instructions for building and pushing via `adb`: `./build.sh workspace/snapio --load`.
