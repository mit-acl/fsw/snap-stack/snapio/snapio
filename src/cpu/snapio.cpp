/**
 * @file snapio.cpp
 * @brief API for accessing ioboard via sDSP
 * @author Parker Lusk <plusk@mit.edu>
 * @date 15 April 2021
 */

#include <chrono>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <type_traits>

#include <io_rpc.h>

#include "snapio/snapio.h"
#include "snapio/misc.h"

namespace acl {
namespace snapio {

static int get_bus(UART uart)
{
#if defined(APQ8074)
  /**
   * in /usr/share/data/adsp/blsp.config:
   * tty-1 bam-9 2-wire
   * tty-2 bam-6 2-wire
   * tty-3 bam-8 2-wire
   * tty-4 bam-2 2-wire
   *
   * n.b., don't use UART port if PWM is using the pins
   *
   * See https://docs.px4.io/master/en/flight_controller/snapdragon_flight.html
   */
  if (uart == UART::J15) return 1;
  if (uart == UART::J13) return 2;
  if (uart == UART::J12) return 3;
  if (uart == UART::J9)  return 4;
#elif defined(APQ8096)
  if (uart == UART::J7)  return 9;
  if (uart == UART::J10) return 7;
  if (uart == UART::J11) return 12;
  if (uart == UART::J12) return 5;
#else
  throw std::runtime_error("Could not resolve UART device from port.");
#endif
}

SnapIO::SnapIO(UART uart, int baud)
: bus_(get_bus(uart)), baud_(baud)
{

  // initialize the RPC read buffer and sioparser
  msgs_buf_ = reinterpret_cast<sio_serial_message_t*>(rpc_shared_mem_alloc(BUF_LEN));
  if (msgs_buf_ == nullptr) {
    throw std::runtime_error("Error allocating shared memory buffers for sioparser");
  }

  if (io_rpc_sioparser_init(bus_, baud_, BUF_LEN)) {
    std::stringstream ss;
    ss << "Error initializing sioparser UART (/dev/tty-" << bus_ << ") @ " << baud_ << " baud";
    throw std::runtime_error(ss.str());
  }

#if defined(APQ8074)
  io_rpc_pwm_init();
#endif

  // start reading in separate thread
  thread_stop_ = false;
  read_thread_ = std::thread(&SnapIO::read_thread, this);
}

// ----------------------------------------------------------------------------

SnapIO::~SnapIO()
{
  thread_stop_ = true;
  if (read_thread_.joinable()) {
    read_thread_.join();
  }

  io_rpc_sioparser_close(bus_);
  rpc_shared_mem_free(reinterpret_cast<uint8_t*>(msgs_buf_));

#if defined(APQ8074)
  io_rpc_pwm_close();
#endif
}

// ----------------------------------------------------------------------------

void SnapIO::set_motors(const std::array<uint16_t, 6>& pwm)
{
  // check that inputs are valid
  for (const auto& usec : pwm) {
    if (usec < PWM_MIN_PULSE_WIDTH) {
      std::stringstream ss;
      ss << "PWM command " << usec << " < " << PWM_MIN_PULSE_WIDTH << " is invalid.";
      throw std::runtime_error(ss.str());
    } else if (usec > PWM_MAX_PULSE_WIDTH) {
      std::stringstream ss;
      ss << "PWM command " << usec << " > " << PWM_MAX_PULSE_WIDTH << " is invalid.";
      throw std::runtime_error(ss.str());
    }
  }

#if defined(APQ8074)
  io_rpc_pwm_update(pwm.data(), pwm.size());
#endif

  sio_serial_motorcmd_msg_t msg;
  memcpy(msg.usec, pwm.data(), sizeof(pwm));

  uint8_t buf[SIO_SERIAL_MAX_MESSAGE_LEN];
  const int len = sio_serial_motorcmd_msg_send_to_buffer(buf, &msg);

  int bytes_written;
  if (io_rpc_uart_write(bus_, buf, len, &bytes_written)) {
    throw std::runtime_error("Error sending motor commands");
  }

  if (bytes_written != len) {
    throw std::runtime_error("Error sending motor commands: incorrect write length");
  }
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void SnapIO::read_thread()
{
  // this is a tight loop around the read function. Read will wait to return
  // untill there is either at least one message available or there was an error
  // this no need to sleep (hooray, no CPU busy-waiting!)
  while (!thread_stop_) {

    //  this blocks for up to 0.5 seconds waiting for a decoded message
    int msgs_read = 0;
    io_rpc_sioparser_read(bus_, reinterpret_cast<uint8_t*>(msgs_buf_), BUF_LEN, &msgs_read);

    // buffer overrun
    if (msgs_read >= BUF_PKT_LEN) {
      std::cerr << "WARNING, sioparser buffer full!" << std::endl;
    }

    // other failure cases
    if (msgs_read < 0) {
      std::cerr << "ERROR exiting uart thread -- maybe someone else using uart?" << std::endl;
      thread_stop_ = true;
      return;
    }

    // handle received messages -- these should be fast!
    if (msgs_read > 0) {
      for (size_t i=0; i<msgs_read; i++) {
        switch (msgs_buf_[i].type) {
          case SIO_SERIAL_MSG_MOTORCMD:
          {
            sio_serial_motorcmd_msg_t msg;
            sio_serial_motorcmd_msg_unpack(&msg, &msgs_buf_[i]);
            // handle_motorcmd_msg(msg);
            break;
          }
        }
      }
    }

  }

}

} // ns snapio
} // ns acl
