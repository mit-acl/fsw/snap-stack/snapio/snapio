/**
 * @file misc.cpp
 * @brief Miscellaneous APIs to support SnapIO APPs<->sDSP comms
 * @author Parker Lusk <plusk@mit.edu>
 * @date 15 April 2021
 */

#include <stdexcept>

#include <rpcmem.h>

#include "snapio/misc.h"

namespace acl {
namespace snapio {

// set to true on call to rpcmem_init
static bool rpcmem_initialized = false;

uint8_t* rpc_shared_mem_alloc(size_t bytes)
{
  uint8_t* ret = nullptr;
  if (!rpcmem_initialized) {
    rpcmem_init();
    rpcmem_initialized = true;
  }

  ret = reinterpret_cast<uint8_t*>(rpcmem_alloc(0, RPCMEM_HEAP_DEFAULT, bytes));
  if (ret == nullptr) {
    throw std::runtime_error("Error in rpc_shared_mem_alloc, rpcmem_alloc failed");
  }

  return ret;
}

// ----------------------------------------------------------------------------

void rpc_shared_mem_free(uint8_t* ptr)
{
  rpcmem_free(ptr);
}

// ----------------------------------------------------------------------------

void voxl_rpc_shared_mem_deinit()
{
  if (rpcmem_initialized) rpcmem_deinit();
  rpcmem_initialized = false;
}

} // ns snapio
} // ns acl